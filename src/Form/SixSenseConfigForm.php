<?php

namespace Drupal\sixsense\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sixsense\SixSenseServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SixSenseConfigForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, MessengerInterface $messager) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sixsense_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      SixSenseServiceInterface::SIXSENSE_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get(SixSenseServiceInterface::SIXSENSE_SETTINGS);
    $keyStorage = $this->entityTypeManager->getStorage('key');

    $form['company_identification_api_token'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Company Identification API token'),
      '#target_type' => 'key',
      '#tags' => FALSE,
      '#default_value' => !is_null($config->get('company_identification_api_token')) ? $keyStorage->load($config->get('company_identification_api_token')) : NULL,
    ];

    $form['lead_scoring_api_token'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Lead Enrichment & Scoring API token'),
      '#target_type' => 'key',
      '#tags' => FALSE,
      '#default_value' => !is_null($config->get('lead_scoring_api_token')) ? $keyStorage->load($config->get('lead_scoring_api_token')) : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(SixSenseServiceInterface::SIXSENSE_SETTINGS);
    $config
      ->set('company_identification_api_token', $form_state->getValue('company_identification_api_token'))
      ->set('lead_scoring_api_token', $form_state->getValue('lead_scoring_api_token'))
      ->save();
    $this->messenger->addMessage($this->t('The tokens have been saved.'));
  }

}
