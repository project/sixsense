<?php

namespace Drupal\sixsense\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\sixsense\SixSenseServiceInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

class SixSenseService implements SixSenseServiceInterface {

  const SIXSENSE_SETTINGS = 'sixsense.settings';

  /**
   * 6sense module config object.
   *
   * @var mixed|string
   */
  protected $config;

  /**
   * Key repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Logger channel for this module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  public function __construct(ConfigFactoryInterface $configFactory, KeyRepositoryInterface $keyRepository, ClientInterface $httpClient, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->config = $configFactory->get(SixSenseServiceInterface::SIXSENSE_SETTINGS);
    $this->keyRepository = $keyRepository;
    $this->httpClient = $httpClient;
    $this->logger = $loggerChannelFactory->get('6sense');
  }

  /**
   * {@inheritDoc}
   */
  public function getCompanyDetails() {
    // Get API token.
    $key_id = $this->config->get('company_identification_api_token');
    $token = $this->keyRepository->getKey($key_id)->getKeyValue();

    // Make request.
    try {
      $response = $this->httpClient->request('GET', 'https://epsilon.6sense.com/v3/company/details', [
        'headers' => [
          'Authorization' => 'Token ' . $token,
        ],
      ]);
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
    }
    if ($response->getStatusCode() == 200) {;
      $data = json_decode($response->getBody());
    }
    else {
      $this->logger->error('6sense API returned error: @message', ['@message' => $response->getBody()]);
      $data = NULL;
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getCompanyFirmographics($params) {
    // Get API token.
    $key_id = $this->config->get('lead_scoring_api_token');
    $token = $this->keyRepository->getKey($key_id)->getKeyValue();

    // Make request.
    try {
      $response = $this->httpClient->request('POST', 'https://scribe.6sense.com/v2/people/enrichment', [
        'headers' => [
          'Authorization' => 'Token ' . $token,
        ],
        'form_params' => $params,
      ]);
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
    }
    if ($response->getStatusCode() == 200) {;
      $data = json_decode($response->getBody());
    }
    else {
      $this->logger->error('6sense API returned error: @message', ['@message' => $response->getBody()]);
      $data = NULL;
    }
    return $data;
  }

}
