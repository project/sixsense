<?php

namespace Drupal\sixsense;

interface SixSenseServiceInterface {

  const SIXSENSE_SETTINGS = 'sixsense.settings';

  /**
   * Gets company information for anonymous users from their IP address.
   * See https://api.6sense.com/docs/#company-identification-api for details.
   *
   * @return mixed
   */
  public function getCompanyDetails();

  /**
   * Gets company firmographic data for the provided email.
   * See https://api.6sense.com/docs/#company-firmographics-api for details.
   *
   * @param $params
   *   An array of parameters to pass to the 6sense Company Firmographics API.
   *
   * @return mixed
   */
  public function getCompanyFirmographics($params);

}
